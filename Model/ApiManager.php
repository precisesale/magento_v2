<?php

namespace Precisesale\Client\Model;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Precisesale\Client\Api\ProductPrice;
use Precisesale\Client\Helper\Data;
use Precisesale\Client\Model\ResourceModel\SyncQueue as SyncQueueResource;
use Precisesale\Client\Model\ResourceModel\SyncQueue\CollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Actions for the API
 */
class ApiManager
{
    /**
     * Limit of items being sent to the API in a single call
     */
    const API_ITEMS_LIMIT = 500;

    /**
     * Product attributes to select on getting collection
     */
    const PRODUCT_ATTRIBUTES_TO_SELECT = [
        'id', 'sku', 'status', 'visibility', 'qty', 'is_in_stock', 'name',
        'meta_title', 'meta_description', 'url_key', 'price', 'description',
        'short_description', 'meta_keyword',
    ];

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var ApiClient
     */
    protected $apiClient;

    /**
     * @var SyncQueueResource
     */
    protected $syncQueueResource;

    /**
     * @var CollectionFactory
     */
    protected $syncQueueCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductResource
     */
    protected $productResource;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * Constructor
     */
    public function __construct(
        LoggerInterface $logger,
        Data $helper,
        ApiClient $apiClient,
        SyncQueueResource $syncQueueResource,
        CollectionFactory $syncQueueCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProductResource $productResource,
        OrderCollectionFactory $orderCollectionFactory
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->apiClient = $apiClient;
        $this->syncQueueResource = $syncQueueResource;
        $this->syncQueueCollectionFactory = $syncQueueCollectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productResource = $productResource;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * Synchronise with precise.sale using Sync Queue table
     *
     * @return void
     *
     * @throws \Exception
     */
    public function synchroniseUsingQueue()
    {
        $queueItems = $this->syncQueueCollectionFactory->create();

        /** @var SyncQueue $queueItem */
        foreach ($queueItems as $queueItem) {
            if ((int) $queueItem->getEntityId() === Data::FLAG_ALL_ITEMS_ID) {
                switch ($queueItem->getEntityName()) {
                    case Data::ENTITY_NAME_PRODUCT:
                        $this->sendAllProducts();
                        break;
                    case Data::ENTITY_NAME_ORDER:
                        $this->sendAllOrders();
                        break;
                }

                $this->syncQueueResource->delete($queueItem);
                continue;
            }

            if ($queueItem->getEntityName() === Data::ENTITY_NAME_PRODUCT) {
                $this->sendProductById((int) $queueItem->getEntityId());
            } elseif ($queueItem->getEntityName() === Data::ENTITY_NAME_ORDER) {
                $this->sendOrderById((int) $queueItem->getEntityId());
            }

            $this->syncQueueResource->delete($queueItem);
        }
    }

    /**
     * Update product prices from precise.sale
     *
     * @return void
     */
    public function updatePrices()
    {
        $productPriceResponse = $this->apiClient->getProductPrices(true);

        $this->updatePricesFromCollection($productPriceResponse->getProductPrices());

        while ($productPriceResponse->getNextLinkHeader() !== null) {
            $productPriceResponse = $this->apiClient->getProductPrices(
                true,
                $productPriceResponse->getNextLinkHeader()
            );

            $this->updatePricesFromCollection($productPriceResponse->getProductPrices());
        }
    }

    /**
     * Update product prices from collection
     *
     * @param ProductPrice[] $productPrices
     *
     * @return void
     */
    private function updatePricesFromCollection(array $productPrices)
    {
        foreach ($productPrices as $productPrice) {
            $this->logger->notice(sprintf('Updating price for Product %d', $productPrice->getId()));
            /** @var Product $product */
            $product = $this->productCollectionFactory->create()->getItemById($productPrice->getId());
            $product->setPrice($productPrice->getProposedPrice());

            try {
                $this->productResource->save($product);
            } catch (\Exception $e) {
                $this->logger->error(sprintf(
                    'Couldn\'t save price %d for product %d',
                    $productPrice->getProposedPrice(),
                    $productPrice->getId()
                ));
            }
        }
    }

    /**
     * Send all orders to the API
     *
     * @return void
     */
    private function sendAllOrders()
    {
        $orderCollection = $this->orderCollectionFactory->create();

        $page = 1;
        $orderCollection->setPageSize(self::API_ITEMS_LIMIT);

        while ($page <= $orderCollection->getLastPageNumber()) {
            $orderCollection->setCurPage($page);
            $orderCollection->clear()->load();
            $this->apiClient->putOrders($orderCollection->getItems());
            $page = $page + 1;
        }
    }

    /**
     * Send all products to the API
     *
     * @return void
     */
    private function sendAllProducts()
    {
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect(self::PRODUCT_ATTRIBUTES_TO_SELECT);

        $page = 1;
        $productCollection->setPageSize(self::API_ITEMS_LIMIT);

        while ($page <= $productCollection->getLastPageNumber()) {
            $productCollection->setCurPage($page);
            $productCollection->clear()->load();
            $this->apiClient->putProducts($productCollection->getItems());
            $page = $page + 1;
        }
    }

    /**
     * Send Product by its ID
     *
     * @param int $id Product Entity ID
     *
     * @return void
     */
    private function sendProductById(int $id)
    {
        /** @var Product $product */
        $product = $this->productCollectionFactory->create()
            ->addAttributeToSelect(self::PRODUCT_ATTRIBUTES_TO_SELECT)
            ->getItemById($id);

        $this->apiClient->putProduct($product);
    }

    /**
     * Send Order by its ID
     *
     * @param int $id Order Entity ID
     *
     * @return void
     */
    private function sendOrderById(int $id)
    {
        /** @var Order $order */
        $order = $this->orderCollectionFactory->create()->getItemById($id);

        $this->apiClient->putOrder($order);
    }
}
