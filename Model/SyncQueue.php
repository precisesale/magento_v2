<?php

namespace Precisesale\Client\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * SyncQueue model
 *
 * @method $this  setEntityName(string $name)
 * @method $this  setCreatedAt(int $timestamp)
 * @method string getEntityName()
 * @method int    getCreatedAt()
 */
class SyncQueue extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'precisesale_sync_queue';

    protected $_cacheTag = 'precisesale_sync_queue';
    protected $_eventPrefix = 'precisesale_sync_queue';

    protected function _construct()
    {
        $this->_init('Precisesale\Client\Model\ResourceModel\SyncQueue');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
