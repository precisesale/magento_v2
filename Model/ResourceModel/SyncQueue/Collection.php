<?php

namespace Precisesale\Client\Model\ResourceModel\SyncQueue;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'precisesale_sync_queue_collection';
    protected $_eventObject = 'sync_queue_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Precisesale\Client\Model\SyncQueue', 'Precisesale\Client\Model\ResourceModel\SyncQueue');
    }
}
