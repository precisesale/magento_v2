<?php

namespace Precisesale\Client\Model\ResourceModel;

class SyncQueue extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('precisesale_sync_queue', 'id');
    }
}
