<?php

namespace Precisesale\Client\Model;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Magento\Catalog\Model\Product;
use Magento\Sales\Model\Order;
use Precisesale\Client\Api\ProductPrice;
use Precisesale\Client\Api\ProductPriceResponse;
use Precisesale\Client\Helper\Api;
use Precisesale\Client\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * API Client
 */
class ApiClient
{
    /**
     * Server URI for precise.sale
     */
    const SERVER_URI = 'https://api.precise.sale';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Api
     */
    protected $apiHelper;

    public function __construct(LoggerInterface $logger, Data $helper, Api $apiHelper)
    {
        $this->client = new Client([
            'base_uri' => self::SERVER_URI,
        ]);

        $this->logger = $logger;
        $this->helper = $helper;
        $this->apiHelper = $apiHelper;
    }

    /**
     * Put single product to the API
     *
     * @param Product $product
     *
     * @return void
     */
    public function putProduct(Product $product)
    {
        $this->logger->notice(sprintf('Putting product %d to precise.sale', $product->getId()));
        $request = new Request('PUT', "product/{$product->getId()}", [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ], json_encode($this->apiHelper->transformProductToApiData($product)));

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Put multiple products to the API
     *
     * @param Product[] $products
     *
     * @return void
     */
    public function putProducts(array $products)
    {
        $this->logger->notice(sprintf('Putting %d products to precise.sale', count($products)));

        $apiProducts = [];
        foreach ($products as $product) {
            $apiProducts[] = $this->apiHelper->transformProductToApiData($product);
        }

        $request = new Request('PUT', "product", [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ], json_encode($apiProducts));

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Delete product from the API
     *
     * @param Product $product
     *
     * @return void
     */
    public function deleteProduct(Product $product)
    {
        $this->logger->notice(sprintf('Deleting product %d from precise.sale', $product->getId()));

        $request = new Request('DELETE', "product/{$product->getId()}", [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ]);

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Put single order to the API
     *
     * @param Order $order
     *
     * @return void
     */
    public function putOrder(Order $order)
    {
        $this->logger->notice(sprintf('Putting order %d to precise.sale', $order->getId()));

        $request = new Request('PUT', "order/{$order->getId()}", [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ], json_encode($this->apiHelper->transformOrderToApiData($order)));

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Put multiple orders to the API
     *
     * @param Order[] $orders
     *
     * @return void
     */
    public function putOrders(array $orders)
    {
        $this->logger->notice(sprintf('Putting %d orders to precise.sale', count($orders)));

        $apiOrders = [];
        foreach ($orders as $order) {
            $apiOrders[] = $this->apiHelper->transformOrderToApiData($order);
        }

        $request = new Request('PUT', "order", [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ], json_encode($apiOrders));

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Get product prices from the API
     *
     * @param bool        $toReprice Get only products to reprice
     * @param string|null $customUri Custom resource URI, for example "Next" URL
     *
     * @return ProductPriceResponse
     */
    public function getProductPrices(bool $toReprice = false, $customUri = null)
    {
        $this->logger->notice(sprintf('Getting product prices from precise.sale'));

        if ($customUri === null) {
            $request = new Request('GET', 'product', [
                'query' => [
                    'to_reprice' => $toReprice,
                ],
                'content-type' => 'application/json',
                'Authorization' => "Bearer {$this->helper->getApiToken()}",
            ]);
        } else {
            $request = new Request('GET', $customUri, [
                'content-type' => 'application/json',
                'Authorization' => "Bearer {$this->helper->getApiToken()}",
            ]);
        }

        try {
            $response = $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            $this->postErrorLog($e->getCode(), $e->getMessage());

            // Log the error but let the script working
            return new ProductPriceResponse([]);
        }

        $nextLink = $this->apiHelper->findNextFromResponse($response);

        $priceElements = json_decode((string) $response->getBody(), true);

        $productPrices = [];
        foreach ($priceElements as $priceElement) {
            $productPrices[] = ProductPrice::createFromArray($priceElement);
        }

        return new ProductPriceResponse($productPrices, $nextLink);
    }

    /**
     * Post error to the API
     *
     * @param int    $errorCode
     * @param string $errorMessage
     *
     * @return void
     */
    public function postErrorLog(int $errorCode, string $errorMessage)
    {
        $this->logger->notice('Sending error log to the API server');

        $request = new Request('POST', 'error_log', [
            'content-type' => 'application/json',
            'Authorization' => "Bearer {$this->helper->getApiToken()}",
        ], json_encode([
            'code' => $errorCode,
            'message' => $errorMessage,
        ]));

        try {
            $this->client->send($request);
        } catch (GuzzleException $e) {
            $this->logger->error('Couln\'t send error log to the API server: ' . $e->getMessage());
        }
    }
}
