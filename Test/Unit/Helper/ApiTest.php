<?php

namespace Precisesale\Client\Test\Unit\Helper;

use GuzzleHttp\Psr7\Response;
use Precisesale\Client\Helper\Api;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{
    /** @var Api */
    public $helper;

    /**
     * Prepare for tests
     */
    protected function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->helper = $objectManager->getObject(Api::class);
    }

    /**
     * @dataProvider nextHeaderProvider
     */
    public function testFindNextFromResponse($headers, $expected)
    {
        $response = new Response(200, $headers);

        $result = $this->helper->findNextFromResponse($response);

        $this->assertEquals($expected, $result);
    }

    public function nextHeaderProvider()
    {
        return [
            [
                // Headers
                [
                    'Link' => [
                        '<https://api.precise.sale/product?to_reprice=true&per_page=2&page=2>; rel="next", <https://api.precise.sale/product?to_reprice=true&per_page=2&page=946>; rel="last"',
                    ]
                ],
                // Expected
                'https://api.precise.sale/product?to_reprice=true&per_page=2&page=2',
            ],
            [
                // Headers
                [
                    'Link' => [
                        null,
                    ]
                ],
                // Expected
                null,
            ],
            [
                // Headers
                [
                    'SomeHeader' => [
                        'blah',
                    ]
                ],
                // Expected
                null,
            ],
        ];
    }
}
