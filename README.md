# precise.sale Magento 2 Client

## Installation

`composer require precisesale/precisesale-magento2`

## Configuration

1. Go to Stores -> Settings -> Configuration -> Sales -> precise.sale
2. Enter your API key from precise.sale in the "Api Token" field
3. Set General Configuration -> "Enable" to "Yes"
4. In CRON tab you can enable automated data transfer by Magento CRON. You can also leave them disabled and
use commands:
- `bin/magento precisesale:sync` - To synchronise queued Products and Orders
- `bin/magento precisesale:update-prices` - To update product prices
5. Confirm your settings with "Save Config" button
6. Use buttons "Synchronise Products" and "Synchronise Orders" to transfer all current products and orders to
precise.sale

