<?php

namespace Precisesale\Client\Helper;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Precisesale\Client\Model\ResourceModel\SyncQueue as SyncQueueResourceModel;
use Precisesale\Client\Model\SyncQueue;
use Precisesale\Client\Model\SyncQueueFactory;

class Data extends AbstractHelper
{
    /**
     * XML path to module enable configuration
     */
    const XML_PATH_CONFIG_ENABLE = 'precisesale/configuration/enable';

    /**
     * XML path to API token configuration
     */
    const XML_PATH_CONFIG_TOKEN = 'precisesale/configuration/token';

    /**
     * XML path to sync CRON enable configuration
     */
    const XML_PATH_CRON_SYNC_ENABLE = 'precisesale/cron/sync_enable';

    /**
     * XML path to update prices CRON enable configuration
     */
    const XML_PATH_CRON_UPDATE_PRICES_ENABLE = 'precisesale/cron/update_prices_enable';

    /**
     * Entity ID that is used as a flag to sync all items
     */
    const FLAG_ALL_ITEMS_ID = 0;

    /**
     * Entity name for orders
     */
    const ENTITY_NAME_ORDER = 'ORDER';

    /**
     * Entity name for products
     */
    const ENTITY_NAME_PRODUCT = 'PRODUCT';

    /**
     * @var SyncQueueFactory
     */
    private $syncQueueFactory;

    /**
     * @var SyncQueueResourceModel
     */
    private $syncQueueResourceModel;

    /**
     * Constructor
     */
    public function __construct(
        Context $context,
        SyncQueueFactory $syncQueueFactory,
        SyncQueueResourceModel $syncQueueResourceModel
    ) {
        $this->syncQueueFactory = $syncQueueFactory;
        $this->syncQueueResourceModel = $syncQueueResourceModel;

        parent::__construct($context);
    }

    /**
     * Check if module is enabled for given store
     *
     * @param int|null $storeId
     *
     * @return bool
     */
    public function isModuleEnabled(int $storeId = null): bool
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_CONFIG_ENABLE, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * Get API token for given store
     *
     * @param int|null $storeId
     *
     * @return string
     */
    public function getApiToken(int $storeId = null): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CONFIG_TOKEN, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * Check if CRON synchronisation is enabled
     *
     * @return bool
     */
    public function isCronSyncEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_CRON_SYNC_ENABLE);
    }

    /**
     * Check if CRON update prices is enabled
     *
     * @return bool
     */
    public function isCronUpdatePricesEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_CRON_UPDATE_PRICES_ENABLE);
    }

    /**
     * Add all orders to the sync queue
     *
     * @return void
     */
    public function addAllOrdersToQueue()
    {
        /** @var SyncQueue $syncQueueItem */
        $syncQueueItem = $this->syncQueueFactory->create();
        $syncQueueItem->setEntityId(self::FLAG_ALL_ITEMS_ID);
        $syncQueueItem->setEntityName(self::ENTITY_NAME_ORDER);
        $syncQueueItem->setCreatedAt(time());

        try {
            $this->syncQueueResourceModel->save($syncQueueItem);
        } catch (\Exception $e) {
            $this->_logger->error('Couldn\'t add all orders to the sync queue');
        }
    }

    /**
     * Add all products to the sync queue
     *
     * @return void
     */
    public function addAllProductsToQueue()
    {
        /** @var SyncQueue $syncQueueItem */
        $syncQueueItem = $this->syncQueueFactory->create();

        $syncQueueItem->setEntityId(self::FLAG_ALL_ITEMS_ID);
        $syncQueueItem->setEntityName(self::ENTITY_NAME_PRODUCT);
        $syncQueueItem->setCreatedAt(time());

        try {
            $this->syncQueueResourceModel->save($syncQueueItem);
        } catch (\Exception $e) {
            $this->_logger->error('Couldn\'t add all products to the sync queue');
        }
    }

    /**
     * Add product to the sync queue by its ID
     *
     * @param int $id Product Entity ID
     *
     * @return void
     */
    public function addProductToQueue(int $id)
    {
        /** @var SyncQueue $syncQueueItem */
        $syncQueueItem = $this->syncQueueFactory->create();

        $syncQueueItem->setEntityId($id);
        $syncQueueItem->setEntityName(self::ENTITY_NAME_PRODUCT);
        $syncQueueItem->setCreatedAt(time());

        try {
            $this->syncQueueResourceModel->save($syncQueueItem);
        } catch (\Exception $e) {
            $this->_logger->error(sprintf('Couldn\'t add product %d to the sync queue', $id));
        }
    }

    /**
     * Add order to the sync queue by its ID
     *
     * @param int $id Order Entity ID
     *
     * @return void
     */
    public function addOrderToQueue(int $id)
    {
        /** @var SyncQueue $syncQueueItem */
        $syncQueueItem = $this->syncQueueFactory->create();
        $syncQueueItem->setEntityId($id);
        $syncQueueItem->setEntityName(self::ENTITY_NAME_ORDER);
        $syncQueueItem->setCreatedAt(time());

        try {
            $this->syncQueueResourceModel->save($syncQueueItem);
        } catch (\Exception $e) {
            $this->_logger->error(sprintf('Couldn\'t add order %d to the sync queue', $id));
        }
    }
}
