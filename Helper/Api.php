<?php

namespace Precisesale\Client\Helper;

use GuzzleHttp\Psr7\Response;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\Product;
use Magento\Sales\Model\Order;

/**
 * Helper for API methods
 */
class Api extends AbstractHelper
{
    /**
     * Transform Product model to API compatible data
     *
     * @param Product $product
     *
     * @return array
     */
    public function transformProductToApiData(Product $product): array
    {
        return [
            'id' => (int) $product->getId(),
            'sku' => $product->getSku(),
            'status' => (int) $product->getStatus(),
            'visibility' => (int) $product->getVisibility(),
            'quantity' => (float) $product->getQty(),
            'is_in_stock' => (bool) $product->isInStock(),
            'name' => $product->getName(),
            'meta_title' => $product->getData('meta_title'),
            'meta_description' => $product->getData('meta_description'),
            'url_key' => $product->getUrlKey(),
            'price' => (float) $product->getPrice(),
            'description' => $product->getData('description'),
            'short_description' => $product->getData('short_description'),
            'meta_keyword' => $product->getData('meta_keyword'),
        ];
    }

    /**
     * Transform Order model to API compatible data
     *
     * @param Order $order
     *
     * @return array
     */
    public function transformOrderToApiData(Order $order): array
    {
        $products = [];
        foreach ($order->getAllItems() as $item) {
            $products[] = [
                'id' => (int) $item->getProductId(),
                'price' => (float) $item->getPriceInclTax(),
                'quantity' => (float) $item->getQtyOrdered(),
            ];
        }

        return [
            'id' => (int) $order->getId(),
            'shipping_description' => $order->getShippingDescription(),
            'is_virtual' => (bool) $order->getIsVirtual(),
            'order_currency_code' => $order->getOrderCurrencyCode(),
            'grand_total' => (float) $order->getGrandTotal(),
            'products' => $products,
        ];
    }

    /**
     * Find "next" URL from response to access next page of listing
     *
     * @param Response $response
     *
     * @return string|null
     */
    public function findNextFromResponse(Response $response)
    {
        $linkHeader = $response->getHeader('Link');

        if (empty($linkHeader)) {
            return null;
        }

        preg_match('/<([^>]+)>;\s*rel=[\'"](next)[\'"]/', $linkHeader[0], $matches);

        return count($matches) ? $matches[1] : null;
    }
}
