<?php

namespace Precisesale\Client\Commands;

use Magento\Framework\App\State;
use Precisesale\Client\Model\ApiManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for manual prices update
 */
class UpdatePrices extends Command
{
    /**
     * @var State
     */
    private $state;

    /**
     * @var ApiManager
     */
    private $apiManager;

    /**
     * Constructor
     */
    public function __construct(State $state, ApiManager $apiManager)
    {
        $this->state = $state;
        $this->apiManager = $apiManager;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('precisesale:update-prices');
        $this->setDescription('Update product prices from precise.sale');
    }

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Set Area Code to emulate CRON
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_CRONTAB);

        $this->apiManager->updatePrices();
    }
}
