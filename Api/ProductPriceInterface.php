<?php

namespace Precisesale\Client\Api;

/**
 * Interface for Product Price
 */
interface ProductPriceInterface
{
    /**
     * Get Product ID
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Get Product current price
     *
     * @return float
     */
    public function getPrice(): float;

    /**
     * Get Product proposed price
     *
     * @return float
     */
    public function getProposedPrice(): float;
}
