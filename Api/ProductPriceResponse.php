<?php

namespace Precisesale\Client\Api;

/**
 * Response on GET "product"
 */
class ProductPriceResponse implements ProductPriceResponseInterface
{
    /**
     * Array of Product Price
     *
     * @var ProductPriceInterface[]
     */
    private $productPrices;

    /**
     * "Next" value from "Link" header
     *
     * @var string|null
     */
    private $nextLinkHeader;

    /**
     * Constructor
     */
    public function __construct(array $productPrices, $nextLinkHeader = null)
    {
        $this->productPrices = $productPrices;
        $this->nextLinkHeader = $nextLinkHeader;
    }

    /**
     * Get array of Product Price
     *
     * @return ProductPriceInterface[]
     */
    public function getProductPrices(): array
    {
        return $this->productPrices;
    }

    /**
     * Get "Next" from "Link" header
     *
     * @return string|null
     */
    public function getNextLinkHeader()
    {
        return $this->nextLinkHeader;
    }
}