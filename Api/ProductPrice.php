<?php

namespace Precisesale\Client\Api;

/**
 * Product Price API response
 */
class ProductPrice implements ProductPriceInterface
{
    /**
     * Product ID
     *
     * @var int
     */
    private $id;

    /**
     * Product current price
     *
     * @var float
     */
    private $price;

    /**
     * Product proposed price
     *
     * @var float
     */
    private $proposedPrice;

    /**
     * Get Product ID
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get Product current price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Get Product proposed price
     *
     * @return float
     */
    public function getProposedPrice(): float
    {
        return $this->proposedPrice;
    }

    /**
     * Create object using array data
     *
     * @param array $data
     *
     * @return ProductPrice
     */
    public static function createFromArray(array $data): ProductPrice
    {
        $object = new self();

        $object->id = $data['id'];
        $object->price = $data['price'];
        $object->proposedPrice = $data['proposed_price'];

        return $object;
    }
}
