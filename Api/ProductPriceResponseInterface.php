<?php

namespace Precisesale\Client\Api;

/**
 * Interface for Product Price API Response
 */
interface ProductPriceResponseInterface
{
    /**
     * Get array of Product Price
     *
     * @return ProductPriceInterface[]
     */
    public function getProductPrices(): array;

    /**
     * Get "Next" from "Link" header
     *
     * @return string|null
     */
    public function getNextLinkHeader();
}
