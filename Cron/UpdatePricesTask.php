<?php

namespace Precisesale\Client\Cron;

use Precisesale\Client\Helper\Data;
use Precisesale\Client\Model\ApiManager;

/**
 * CRON task for update product prices
 */
class UpdatePricesTask
{
    /**
     * @var ApiManager
     */
    protected $apiManager;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Constructor
     */
    public function __construct(ApiManager $apiManager, Data $helper)
    {
        $this->apiManager = $apiManager;
        $this->helper = $helper;
    }

    /**
     * Execute the script
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function execute()
    {
        if (!$this->helper->isCronUpdatePricesEnabled() || !$this->helper->isModuleEnabled()) {
            return $this;
        }

        $this->apiManager->updatePrices();

        return $this;
    }
}
