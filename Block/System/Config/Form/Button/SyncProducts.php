<?php

namespace Precisesale\Client\Block\System\Config\Form\Button;

class SyncProducts extends AbstractButton
{
    /**
     * Get URL to controller action for button
     *
     * @return string
     */
    public function getControllerUrl(): string
    {
        return 'precisesale/sync/products';
    }

    /**
     * Get button name to display in the administration panel
     *
     * @return string
     */
    public function getButtonName(): string
    {
        return 'Synchronise Products';
    }

    /**
     * Get button ID for HTML rendering
     *
     * @return string
     */
    public function getButtonId(): string
    {
        return 'addbutton_sync-products';
    }
}
