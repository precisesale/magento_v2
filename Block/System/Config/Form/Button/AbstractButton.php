<?php

namespace Precisesale\Client\Block\System\Config\Form\Button;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

abstract class AbstractButton extends Field
{
    const BUTTON_TEMPLATE = 'Precisesale_Client::system/config/button.phtml';

    /**
     * Render button
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl($this->getControllerUrl());
    }

    /**
     * Get URL to controller action for button
     *
     * @return string
     */
    abstract public function getControllerUrl(): string;

    /**
     * Get button name to display in the administration panel
     *
     * @return string
     */
    abstract public function getButtonName(): string;

    /**
     * Get button ID for HTML rendering
     *
     * @return string
     */
    abstract public function getButtonId(): string;

    /**
     * Set template to itself
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(self::BUTTON_TEMPLATE);
        }

        return $this;
    }

    /**
     * Get the button and scripts contents
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => $this->getButtonId(),
                'label' => __($this->getButtonName()),
            ]
        );

        return $button->toHtml();
    }
}
