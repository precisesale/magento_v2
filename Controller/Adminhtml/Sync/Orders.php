<?php

namespace Precisesale\Client\Controller\Adminhtml\Sync;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Precisesale\Client\Helper\Data;

class Orders extends Action
{
    protected $resultJsonFactory;

    protected $helper;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Data $helper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;

        parent::__construct($context);
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $this->helper->addAllOrdersToQueue();
        $result = $this->resultJsonFactory->create();

        return $result->setData(['success' => true]);
    }
}
