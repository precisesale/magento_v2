<?php

namespace Precisesale\Client\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Precisesale\Client\Helper\Data;

/**
 * Observer for "sales_order_save_after" event
 */
class OrderSaveAfter implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Constructor
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        /** @var Order $order */
        $order = $observer->getOrder();
        $this->helper->addOrderToQueue($order->getId());
    }
}
