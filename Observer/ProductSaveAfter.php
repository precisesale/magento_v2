<?php

namespace Precisesale\Client\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Precisesale\Client\Helper\Data;

/**
 * Observer for "catalog_product_save_after" event
 */
class ProductSaveAfter implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Constructor
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        /** @var Product $product */
        $product = $observer->getProduct();
        $this->helper->addProductToQueue($product->getId());
    }
}
